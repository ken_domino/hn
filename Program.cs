﻿using System;
using System.Threading;
using System.Collections.Generic;
using Nancy;
using Nancy.Hosting.Self;

namespace hn
{
    public class SampleModule : Nancy.NancyModule
    {
        public SampleModule()
        {
            Get["/"] = _ => "Hello World!";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            String value = Environment.GetEnvironmentVariable("PORT");
            if (value == null || value.Equals(""))
                value = "5000";
            var uri = new Uri("http://localhost:" + value);
            List<Uri> list_uri = new List<Uri>();
            list_uri.Add(uri);

            HostConfiguration hostConfigs = new HostConfiguration()
            {
                UrlReservations = new UrlReservations() { CreateAutomatically = true }
            };

            using (NancyHost host = new NancyHost(new DefaultNancyBootstrapper(), hostConfigs, list_uri.ToArray()))
            {
                host.Start();
                foreach (Uri u in list_uri)
                    Console.WriteLine(u);
                TimeSpan t = new TimeSpan(0, 0, 3);
                for (;;)
                    Thread.Sleep(t);
                Console.WriteLine("Press any [Enter] to close the host.");
                Console.ReadLine();
                Console.WriteLine("Ending");
            }
        }
    }
}
